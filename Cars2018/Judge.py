from collections import namedtuple
from pathlib import Path

Ride = namedtuple("Ride", "a b x y s f")
Car = namedtuple("Car", "size rides")


def convert(dictionary):
    return namedtuple('GenericDict', dictionary.keys())(**dictionary)


def parseInput(file: Path):
    file = open(file)
    infos = list(map(int, file.readline().split(" ")))
    rides = tuple(map(lambda r: Ride(*(map(int, filter(lambda x: x != '', r.strip("\n").split(" "))))), file))

    return convert({'R': infos[0],
                    'C': infos[1],
                    'F': infos[2],
                    'N': infos[3],
                    'B': infos[4],
                    'T': infos[5],
                    'rides': rides})


def parseOutput(file: Path):
    file = open(file)

    return tuple(
        map(
            lambda c: Car(c[0], c[1:]),
            map(
                lambda c: list(map(int, filter(lambda x: x != '', c.strip("\n").split(" ")))), file)))


def distance(x, y, a, b):
    return abs(x - a) + abs(y - b)


def rankCar(car: Car, score: int, rides, input):
    """
    rank a car and stores it's rides
    :param car: Car
    :param score: Int
    :param rides: ride list
    :param input: treated input file
    :return: carscore and updated rides, if rides is None carscore contains an error.
    """
    carScore = 0
    carPosition = [0, 0]
    carRides = rides[:]
    t = 0
    if car.size != len(car.rides): return ("Incorect ride count", None)
    for ride in car.rides:
        ride = input.rides[ride]
        if ride in carRides: return ("A ride can only be treated once", None)

        value = distance(ride.a, ride.b, ride.x, ride.y)
        gettingTo = distance(ride.a, ride.b, *carPosition)
        if t + gettingTo + value <= input.T:
            if t + gettingTo + value <= ride.f: score += value
            if t + gettingTo < ride.s: score += input.B

        carRides.append(ride)
        carPosition = [ride.x, ride.y]
        t += gettingTo + value
    return score, carRides

def rank(input, output):
    # print("game has {} rides with {} bonnus and {} time".format(input.R,input.B,input.T))
    score = 0
    rides = []
    if input.F > len(output): return "You should provide a schedule for all you fleet"
    if input.F < len(output): return "Number of rides should not be more than the specified input"
    for car in output:
        score, rides = rankCar(car, score, rides, input)
        if rides is None:
            return score
    return score


if __name__ == '__main__':

    # this script will use all inputs file in the folder ./inputs and try and compute scores with output folders
    # output folder must include the inputname in them, and be in ./outputs
    #optionally a caller can parse it's on files with parseInput and parseOutput, and score them with rank

    inputs = Path("./inputs")
    outputs = Path("./outputs")
    for inFile in inputs.iterdir():
        print("Scores for {}".format(inFile.name))
        dataIn = parseInput(inFile)
        for outFile in outputs.glob("*{}*".format(inFile.name.split(".")[0])):
            dataOut = parseOutput(outFile)
            score = rank(dataIn, dataOut)
            print("Output {} scores {}".format(outFile.name, score))
